var  controller  = new ScrollMagic . Contrôleur ( ) ; 


var scene = new ScrollMagic . Scene({
    triggerElement: '.starwars-header'
})

    .setClassToggle('.starwars-header', 'fade-in')
    .addIndicators({
        name: 'INDICATIONS',
        indent: 200,
        colorStart: '#fff'
    });